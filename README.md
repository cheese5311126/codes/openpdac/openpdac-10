# OpenPDAC-10

[![GitLab Release (latest by SemVer)](https://img.shields.io/github/v/release/demichie/OpenPDAC-10)](https://github.com/demichie/OpenPDAC-10/releases)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7701703.svg)](https://doi.org/10.5281/zenodo.7701703)

OpenPDAC is an offline Eulerian-Lagrangian model for the simulation of volcanic fluids. The model solves the
conservation equations of mass, momentum and energy for a Eulerian-Eulerian compressible multiphase mixture of
gasses and fine (up to mm size) particles, and the Lagrangian transport equations for coarser solid particles. The solver is
based on the equations of the PDAC model, ported and optimized on the open-source OpenFOAM C++ toolbox for
Computational Fluid Dynamics (CFD).

## Documentation

## Developers

## License

## Copyright
